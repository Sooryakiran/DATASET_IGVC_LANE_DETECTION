import cv2
import numpy as np 
import os
import matplotlib.pyplot as plt
_file=open("now.txt","r")

i=int(_file.read())+1
_write=open("now.txt","w")

total_images=223
draw =False
ix,iy = -1,-1
leftx=[]
lefty=[]
rightx=[]
righty=[]
def draw_circlel(event,x,y,flags,param):
    global leftx,lefty,rightx,righty
    global ix,iy
    global draw
    if event == cv2.EVENT_LBUTTONDOWN:
      draw=True
    elif event == cv2.EVENT_MOUSEMOVE:
      if draw==True:
        cv2.circle(img,(x,y),20,(255,0,0),-1)
        ix,iy = x,y
        leftx.append(x)
        lefty.append(y)
    elif event == cv2.EVENT_LBUTTONUP:
        draw=False
def draw_circler(event,x,y,flags,param):
    global leftx,lefty,rightx,righty
    global ix,iy
    global draw
    if event == cv2.EVENT_LBUTTONDOWN:
      draw=True
    elif event == cv2.EVENT_MOUSEMOVE:
      if draw==True:
        cv2.circle(img,(x,y),20,(0,0,255),-1)
        ix,iy = x,y    
        rightx.append(x)
        righty.append(y)
    elif event == cv2.EVENT_LBUTTONUP:
        draw=False    



for j in range(i,total_images):
    global leftx,lefty,rightx,righty
    leftx=[]
    lefty=[]
    rightx=[]
    righty=[]
    pg=j/222*100
    os.system('clear')
    print("Labelling Lanes")
    print('Progress :\t' +"{:10.2f}".format(pg)+"% ")
    print("Image " +str(j)+"/"+str(total_images))
    image=cv2.imread("data/"+str(j)+".png")
    #cv2.imshow("WINDOW",image)
    
    _write.seek(0,0)
    _write.write(str(j))
    _write.flush()
    img =cv2.resize(image,(1000,1000))
    
    cv2.namedWindow('left')
    cv2.setMouseCallback('left',draw_circlel)

    while(1):
        cv2.imshow('left',img)
        if cv2.waitKey(1) & 0xFF == ord(' '):
            break
    cv2.destroyAllWindows()
    cv2.namedWindow('right')   
    cv2.setMouseCallback('right',draw_circler)
    
    while(1):
        cv2.imshow('right',img)
        if cv2.waitKey(1) & 0xFF == ord(' '):
            break
    cv2.destroyAllWindows()
    fh=open("csv.csv","a")
    string=str(j)+".png"
    string+="\t"
    lanes=img*0
    y=np.linspace(0,1000,1000)
    if len(leftx)==0:
        string+="0\t0\t0\t0\t0\t0\t"
    else:
        
        left_lane=np.polyfit(lefty,leftx,2)
        x=left_lane[0]*y*y+left_lane[1]*y+left_lane[2]
        string+="1\t" + str(left_lane[0])+"\t" +str(left_lane[1]) + "\t" +str(left_lane[2]) +"\t" + str(np.amin(lefty)) + "\t" + str(np.amax(lefty)) + "\t"
        for kk in range(np.amin(lefty),np.amax(lefty)):
            cv2.circle(lanes,(int(x[kk]),int(y[kk])),10,(255,255,0),-1)



    if len(rightx)==0:
        string+="0\t0\t0\t0\t0\t0\t"
    else:
        
        right_lane=np.polyfit(righty,rightx,2)
        xr=right_lane[0]*y*y+right_lane[1]*y+right_lane[2]

        string+="1\t" +str(right_lane[0])+"\t" +str(right_lane[1]) + "\t" +str(right_lane[2]) +"\t" + str(np.amin(righty)) + "\t" + str(np.amax(righty)) + "\t"
        for kk in range(np.amin(righty),np.amax(righty)):
            cv2.circle(lanes,(int(xr[kk]),int(y[kk])),10,(0,255,255),-1)

   
    cv2.imshow("LANES",lanes)
    cv2.waitKey(0)
    print(string)
    fh.write(string)
    fh.write("\n")
    fh.close()
_write.close()
_file.close()