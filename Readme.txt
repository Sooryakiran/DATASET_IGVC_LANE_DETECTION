This is an incomplete dataset containing manually labelled lanes for IGVC autonav challenge.

At present it contains only 222 images which are taken from UNSW's 2010 IGVC run available online
These images are present inside the /data/ folder

Lanes are represented by polynomials of the form:
x=a*y**2 + b*y + c
Following OpenCV conventions (Top Left= Origin, X+ = Horizontal_right, Y+=Vertical_bottom)
There is a lanes.csv file (delimiter= '\t') which contains:
example:

image_filename  LeftlanePresent?(0/1) Leftlane_coeff_a  Leftlane_coeff_b    Leftlane_coeff_c    Leftlane_min_y  leftlane_max_y  RightlanePresent?(0/1)  Rightlane_coeff_a   Rightlane_coeff_b   Rightlane_coeff_c   Righlane_max_y  Righlane_min_y

1.png           0                     0                 0                   0                   0               0               1                       21.45               3.343               232.2323            320             1   

Left_lane absent
Right_lane present with x=21.45*y**2 + 3.343*y + 232.2323 between y=1 and y=320

NOTE: FOR UNIFORMITY, ALL THE IMAGES HAS TO BE CONVERTED TO 1000 X 1000 BEFORE USING THE ABOVE DATA

To Manually Label the data, U can use the labeller.py file --Undocumented